-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 05, 2014 at 01:01 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `carstar`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(55) NOT NULL,
  `standrad fee` varchar(255) NOT NULL,
  `discount fee` varchar(255) NOT NULL,
  `late fee` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `model_id`, `standrad fee`, `discount fee`, `late fee`) VALUES
(1, 1, '100', '80', '120'),
(2, 2, '60', '40', '80'),
(3, 3, '90', '70', '110');

-- --------------------------------------------------------

--
-- Table structure for table `carsdetails`
--

CREATE TABLE IF NOT EXISTS `carsdetails` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `carType` varchar(45) DEFAULT NULL,
  `lisenceNo` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `carsdetails`
--

INSERT INTO `carsdetails` (`c_id`, `carType`, `lisenceNo`, `status`, `color`) VALUES
(1, 'suzuki swift', '218468', 'e(engaged)', 'blue'),
(2, 'subaru station', '135496', 'a(available)', 'red'),
(3, 'Jeep', '5148-536', 'e', 'silver');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='		' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `c_id`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `models`
--

CREATE TABLE IF NOT EXISTS `models` (
  `model_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `year` varchar(55) NOT NULL,
  PRIMARY KEY (`model_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `models`
--

INSERT INTO `models` (`model_id`, `name`, `year`) VALUES
(1, 'Suzuki Swift ', '2012'),
(2, 'Subaru Station', '1991'),
(3, 'Suzuki Baleno', '2010 ');

-- --------------------------------------------------------

--
-- Table structure for table `userstypes`
--

CREATE TABLE IF NOT EXISTS `userstypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userPosition` varchar(255) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `userstypes`
--

INSERT INTO `userstypes` (`id`, `userPosition`, `name`) VALUES
(1, 'Administrator', 'Noa Yom Tov'),
(2, 'Branch Manager', 'Niki Lewis'),
(3, 'Clerk', 'Shlomo Nisimyan'),
(4, 'Customer', 'Milly Harris'),
(5, 'Anonymous', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
